﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace CSharpMoveDirectory
{
    class Program
    {
        static void Main(string[] args)
        {
            var currDir = Directory.GetCurrentDirectory();
            var srcDir = Path.Combine(currDir, "SrcDir");
            var dstDir = Path.Combine(currDir, "DstDir");
            Console.WriteLine($"SrcDir exists={Directory.Exists(srcDir)}");
            Console.WriteLine($"DstDir exists={Directory.Exists(dstDir)}");

            Directory.CreateDirectory(srcDir);
            Console.WriteLine($"SrcDir exists={Directory.Exists(srcDir)}");

            Directory.Move(srcDir, dstDir);
            Console.WriteLine($"SrcDir exists={Directory.Exists(srcDir)}");
            Console.WriteLine($"DstDir exists={Directory.Exists(dstDir)}");

            Directory.Delete(dstDir);
            Console.WriteLine($"DstDir exists={Directory.Exists(dstDir)}");
        }
    }
}
